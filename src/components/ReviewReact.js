import React, {Component} from "react";
import '../App.css';

class ReviewReact extends Component {
    constructor(props) {
      super(props)
  
      this.state = {
        Height: 100,
        Width: 100,
        value: 10,
        color: "blue",
        radius: 0
      }
  
      this.incrementSize = this.incrementSize.bind(this);
      this.decrementSize = this.decrementSize.bind(this);
      this.ChangeToCircleOrSquare = this.ChangeToCircleOrSquare.bind(this);
     
    }
    incrementSize() {
      let stateObject = {};
  
      this.setState((previousState) => {
  
        stateObject["Height"] = parseInt(previousState.Height) + parseInt(previousState.value) + "px";
        stateObject["Width"]= parseInt(previousState.Width) + parseInt(previousState.value) + "px";
        return stateObject;
      })
    }
  
    decrementSize() {
      let stateObject = {};
  
      this.setState((previousState) => {
  
        stateObject["Height"] = parseInt(previousState.Height) - parseInt(previousState.value) + "px";
        stateObject["Width"] = parseInt(previousState.Width) - parseInt(previousState.value) + "px";
  
        return stateObject;
      })
    }
  
    ChangeToCircleOrSquare(element) {
    this.setState({
        radius: element
    })
      
    }
  
    
    render() {
      return (
        <div>
          <div style={{height: this.state.Height, backgroundColor: this.state.color, width: this.state.Width, borderRadius: this.state.radius}}>
  
          </div>
          <div>
            <button onClick={this.incrementSize}>increment</button>
            <button onClick={this.decrementSize}>decrement</button>
            <button onClick={() => {this.ChangeToCircleOrSquare("50%")}}>Change To Circle</button>
            <button onClick={() => {this.ChangeToCircleOrSquare("0")}}>Change To Square</button>
          </div>
        </div>
      )
    }
  }


export default ReviewReact  